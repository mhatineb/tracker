CREATE TABLE IF NOT EXISTS user (
    id INT PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(30),
    email VARCHAR(100) NOT NULL,
    password VARCHAR(100) NOT NULL,
    role VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS activity (
    id INT PRIMARY KEY AUTO_INCREMENT,
    label VARCHAR(100),
    idUser INT,
    FOREIGN KEY (idUser) REFERENCES user (id) ON DELETE CASCADE,
    UNIQUE (label, idUser)
);


CREATE TABLE IF NOT EXISTS dailyNews (
    id INT PRIMARY KEY AUTO_INCREMENT,
    idUser INT,
    idActivity INT,
    date DATE,
    rating INT CHECK (rating BETWEEN 1 AND 5),
    FOREIGN KEY (idUser) REFERENCES user (id) ON DELETE CASCADE,
    FOREIGN KEY (idActivity) REFERENCES activity (id) ON DELETE CASCADE
);

ALTER TABLE dailyNews
ADD CONSTRAINT unique_activity_date UNIQUE (idActivity, date);

CREATE TABLE IF NOT EXISTS picture (
    id INT PRIMARY KEY AUTO_INCREMENT,
    src VARCHAR(255),
    day DATE,
    idUser INT,
    FOREIGN KEY (idUser) REFERENCES user (id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS invite (
    id INT PRIMARY KEY AUTO_INCREMENT,
    status ENUM('pending', 'accepted', 'refused'),
    sender INT,
    FOREIGN KEY (sender) REFERENCES user (id) ON DELETE CASCADE,
    recipient INT,
    FOREIGN KEY (recipient) REFERENCES user (id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS user_activity (
    idUser INT,
    idActivity INT,
    PRIMARY KEY (idUser, idActivity),
    FOREIGN KEY (idUser) REFERENCES user (id) ON DELETE CASCADE,
    FOREIGN KEY (idActivity) REFERENCES activity (id) ON DELETE CASCADE
);


INSERT INTO `user` VALUES
(1,'marie','marie@test.com','$2a$12$fHvzXFeIXWpOfNZcrJKewO5SpuRbjq6QALYzIvEY2rUbzM4eb8N9O','ROLE_USER'),
(2,'test','test@test.com','$2a$12$StX6s6rMuFFG2CQpGw6H4eWvALO2z6usmxrv9o5iW2y2FTSj22Qm2','ROLE_USER');

INSERT INTO `activity` VALUES
(1,'randonnée',1),
(2,'badmington',1),
(3,'bilboquet',1),
(4,'cinema',1),
(5,'cross-fit',2),
(6,'jeux videos',1),
(7,'lecture',1),
(8,'bricolage',1),
(9,'natation',1),
(10,'cuisine',1);

DROP TABLE IF EXISTS `dailyNews`;

INSERT INTO `dailyNews` VALUES
(5,1,1,'2024-12-16',3),
(8,1,1,'2025-01-05',5),
(9,2,1,'2024-12-30',4),
(10,1,2,'2024-12-22',4),
(11,2,3,'2024-12-31',3),
(12,1,1,'2025-01-01',4),
(13,2,2,'2024-12-09',3),
(14,1,2,'2024-12-24',4),
(15,1,5,'2024-12-31',3),
(16,2,1,'2024-11-04',5),
(17,1,1,'2024-11-06',4),
(18,1,1,'2024-10-07',3),
(19,2,1,'2024-09-09',2),
(20,1,1,'2024-09-10',2),
(21,1,1,'2024-09-18',5),
(22,1,1,'2024-09-12',5),
(23,1,1,'2024-09-13',5),
(24,2,6,'2025-01-07',4),
(25,1,7,'2025-01-06',3),
(26,2,2,'2025-01-07',5),
(27,1,3,'2024-12-24',4),
(28,2,8,'2025-01-02',5),
(29,1,2,'2025-01-08',5),
(32,2,2,'2024-01-08',4),
(33,1,2,'2024-01-09',4),
(34,1,2,'2024-01-24',3),
(35,2,5,'2024-02-13',5),
(36,1,5,'2024-03-22',5),
(37,2,7,'2024-04-12',5),
(38,2,7,'2024-05-15',5),
(39,1,7,'2024-05-17',2),
(40,1,10,'2024-06-07',5),
(41,2,10,'2024-07-08',5),
(42,1,1,'2024-07-09',5),
(43,2,7,'2024-07-24',2),
(44,1,10,'2025-01-13',5),
(45,2,5,'2025-01-14',5),
(46,1,7,'2025-01-15',4),
(47,2,10,'2025-01-16',5),
(48,1,1,'2025-01-14',3),
(49,2,1,'2025-01-07',3),
(50,1,1,'2025-01-20',3);
DROP TRIGGER IF EXISTS AfterInviteAccepted;

CREATE TRIGGER AfterInviteAccepted
AFTER UPDATE ON invite
FOR EACH ROW 
    BEGIN
        IF NEW.status = 'accepted' THEN
            INSERT INTO user_activity (idUser, idActivity)
            SELECT NEW.sender, a.id
            FROM activity a
            WHERE a.idUser = NEW.recipient;
        END IF;
    END;

DROP VIEW IF EXISTS user_monthly_mood;
CREATE VIEW user_monthly_mood AS
SELECT idUser, 
       YEAR(date) AS year,
       MONTH(date) AS month, 
       AVG(rating) AS avg_rating
FROM dailyNews
GROUP BY idUser, YEAR(date), MONTH(date);