# Variables
DOCKER_COMPOSE = docker compose
DOCKER_COMPOSE_FILE = docker-compose.yml
DOCKER_COMPOSE_OVERRIDE = docker-compose.override.yml

# Messages (avec couleurs)
SUCCESS = \033[0;32m[SUCCESS]\033[0m
INFO = \033[0;36m[INFO]\033[0m
ERROR = \033[0;31m[ERROR]\033[0m

# Cibles
.PHONY: all clean package build start stop restart logs deploy

# Nettoyage Maven
clean:
	@echo "$(INFO) Nettoyage du projet..."
	mvn clean
	@echo "$(SUCCESS) Nettoyage terminé."

# Build Maven
package:
	@echo "$(INFO) Compilation du projet..."
	mvn package
	@echo "$(SUCCESS) Compilation terminée."

# Build Docker (uniquement en local)
build:
	@echo "$(INFO) Build de l'image locale..."
	$(DOCKER_COMPOSE) build
	@echo "$(SUCCESS) Build terminé."

# Lancement des services Docker
start:
	@echo "$(INFO) Démarrage des services Docker..."
	$(DOCKER_COMPOSE) up -d
	@echo "$(SUCCESS) Services démarrés."

# Arrêt des services Docker
stop:
	@echo "$(INFO) Arrêt des services Docker..."
	$(DOCKER_COMPOSE) down
	@echo "$(SUCCESS) Services arrêtés."

# Redémarrer les services
restart: stop start

# Afficher les logs des services
logs:
	@echo "$(INFO) Logs des services..."
	$(DOCKER_COMPOSE) logs -f

# Déploiement CI/CD (GitLab)
deploy:
	@echo "$(INFO) Déploiement en cours..."
	$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) up -d --build
	@echo "$(SUCCESS) Déploiement terminé."

# Aide
help:
	@echo "$(INFO) Commandes disponibles :"
	@echo "  make clean     -> Nettoyer le projet"
	@echo "  make package   -> Compiler le projet"
	@echo "  make build     -> Construire l'image Docker (local)"
	@echo "  make start     -> Démarrer les services Docker"
	@echo "  make stop      -> Arrêter les services Docker"
	@echo "  make restart   -> Redémarrer les services"
	@echo "  make logs      -> Afficher les logs"
	@echo "  make deploy    -> Déployer l'application (GitLab CI/CD)"
	@echo "  make help      -> Afficher cette aide"