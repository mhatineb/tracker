CREATE TABLE IF NOT EXISTS user (
    id INT PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(30),
    email VARCHAR(100) NOT NULL,
    password VARCHAR(100) NOT NULL,
    role VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS activity (
    id INT PRIMARY KEY AUTO_INCREMENT,
    label VARCHAR(100),
    idUser INT,
    FOREIGN KEY (idUser) REFERENCES user (id) ON DELETE CASCADE,
    UNIQUE (label, idUser)
);

CREATE TABLE IF NOT EXISTS dailyNews (
    id INT PRIMARY KEY AUTO_INCREMENT,
    idUser INT,
    idActivity INT,
    date DATE,
    rating INT CHECK (rating BETWEEN 1 AND 5),
    FOREIGN KEY (idUser) REFERENCES user (id) ON DELETE CASCADE,
    FOREIGN KEY (idActivity) REFERENCES activity (id) ON DELETE CASCADE
);

ALTER TABLE dailyNews
ADD CONSTRAINT unique_activity_date UNIQUE (idActivity, date);

CREATE TABLE IF NOT EXISTS picture (
    id INT PRIMARY KEY AUTO_INCREMENT,
    src VARCHAR(255),
    day DATE,
    idUser INT,
    FOREIGN KEY (idUser) REFERENCES user (id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS invite (
    id INT PRIMARY KEY AUTO_INCREMENT,
    status ENUM('pending', 'accepted', 'refused'),
    sender INT,
    FOREIGN KEY (sender) REFERENCES user (id) ON DELETE CASCADE,
    recipient INT,
    FOREIGN KEY (recipient) REFERENCES user (id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS user_activity (
    idUser INT,
    idActivity INT,
    PRIMARY KEY (idUser, idActivity),
    FOREIGN KEY (idUser) REFERENCES user (id) ON DELETE CASCADE,
    FOREIGN KEY (idActivity) REFERENCES activity (id) ON DELETE CASCADE
);
