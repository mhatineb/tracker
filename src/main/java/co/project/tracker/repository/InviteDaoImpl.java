package co.project.tracker.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.project.tracker.entities.Invite;
import co.project.tracker.repository.interfaces.CrudDao;

@Repository
public class InviteDaoImpl implements CrudDao<Invite> {
    private static final Logger logger = LoggerFactory.getLogger(InviteDaoImpl.class);

    @Autowired
    private DataSource dataSource;

    @Override
    public boolean add(Invite invite) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO invite (status, sender, recipient) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1,invite.getStatus());
            stmt.setInt(2, invite.getSender());
            stmt.setInt(3, invite.getRecipient());
            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            rs.next();
            invite.setId((rs.getInt(1)));
            return true;
            
    } catch (SQLException e){
        logger.error("Failed to add invite due to SQL exception", e);

    }
        return false;
    }

    @Override
    public List<Invite> getAll() {
        List<Invite> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM invite");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                Invite invite = sqlToInvite(rs);
                list.add(invite);

            }
        } catch (SQLException e) {
            logger.error("Failed to getAll invite due to SQL exception", e);

        }
        return list;
    }

    @Override
    public Invite getById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM invite WHERE id=?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                Invite invite = sqlToInvite(rs);
                return invite;

            }
        } catch (SQLException e) {
            logger.error("Failed to getById invite due to SQL exception", e);

        }
        return null;    
    }

    private Invite sqlToInvite(ResultSet rs) throws SQLException {
        return new Invite(
            rs.getInt("id"),
            rs.getInt("recipient"),
            rs.getInt("sender"),
            rs.getString("status")    
        );
    }

    @Override
    public boolean update(Invite invite) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE invite SET status=?,sender=?,recipient=? WHERE id=?");
            stmt.setInt(1, (int) invite.getSender());
            stmt.setInt(2, (int) invite.getRecipient());
            stmt.setString(3, invite.getStatus());

            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            logger.error("Failed to update invite due to SQL exception", e);

        }
        return false;    
    }

    @Override
    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM invite WHERE id=?");
            stmt.setInt(1,id);

            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            logger.error("Failed to delete invite due to SQL exception", e);

        }
        return false;    }

    
}
