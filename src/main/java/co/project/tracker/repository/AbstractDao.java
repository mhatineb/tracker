package co.project.tracker.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public abstract class AbstractDao<E> {
    private static final Logger logger = LoggerFactory.getLogger(AbstractDao.class);

    @Autowired
    protected DataSource dataSource;

    protected String addSQL;
    protected String getAllSQL;
    protected String getByIdSQL;
    protected String updateSQL;
    protected String deleteSQL;

    AbstractDao(String addSQL, String getAllSQL, String getByIdSQL, String updateSQL, String deleteSQL) {
        this.addSQL = addSQL;
        this.getAllSQL = getAllSQL;
        this.getByIdSQL = getByIdSQL;
        this.updateSQL = updateSQL;
        this.deleteSQL = deleteSQL;
    }

    public List<E> getAll() {
        List<E> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(getAllSQL);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                E entity = sqlToEntity(rs);
                list.add(entity);
            }
        } catch (SQLException e) {
            logger.error("Failed to connect to the database", e);
        }
        return list;
    }

    public E getById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(getByIdSQL);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                E entity = sqlToEntity(rs);
                return entity;
            }
        } catch (SQLException e) {
            logger.error("Failed to connect to the database", e);
        }
        return null;
    }

    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(deleteSQL);
            stmt.setInt(1,id);

            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            logger.error("Failed to connect to the database", e);
        }
        return false;
    }

    public boolean add(E entity) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(addSQL, Statement.RETURN_GENERATED_KEYS);
            entityBindValues(stmt,entity);
            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            rs.next();
            setEntityId(entity, rs.getInt(1));
            

            return true;
        } catch (SQLException e) {
            logger.error("Failed to connect to the database", e);
        }
        return false;
    }

    public boolean update(E entity) {       
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(updateSQL);
            entityBindValuesWithId(stmt,entity);

            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            logger.error("Failed to connect to the database", e);        }
        return false;
    }

    protected abstract void setEntityId(E entity, int id);

    protected abstract void entityBindValues(PreparedStatement stmt, E entity)  throws SQLException ;
    
    protected abstract void entityBindValuesWithId(PreparedStatement stmt, E entity)  throws SQLException ;
    
    protected abstract E sqlToEntity(ResultSet rs) throws SQLException;


}
