package co.project.tracker.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import co.project.tracker.entities.Activity;
import co.project.tracker.exception.CustomExceptions;
import co.project.tracker.repository.interfaces.CrudDao;
import co.project.tracker.service.ActivityService;

@Repository
public class ActivityDaoImpl implements CrudDao<Activity> {
    private static final Logger logger = LoggerFactory.getLogger(ActivityDaoImpl.class);

    private final DataSource dataSource;
    private final DailyNewsDaoImpl dailyNewsRepo;

    public ActivityDaoImpl(DataSource dataSource, DailyNewsDaoImpl dailyNewsRepo) {
        this.dataSource = dataSource;
        this.dailyNewsRepo = dailyNewsRepo;
    }

    /**
     * Récupère la liste des activités associées à un utilisateur donné.
     *
     * @param userId L'ID de l'utilisateur dont on veut récupérer les activités.
     * @return Une liste d'objets {@link Activity} contenant les activités de l'utilisateur.
     * @throws CustomExceptions.DataAccessException En cas d'erreur lors de l'accès à la base de données.
     *
     * Voir {@link ActivityService#getUserActivities(int)}
     */
    public List<Activity> getMyActivities(int userId) {
        String sql = "SELECT * FROM activity WHERE idUser = ?";
        List<Activity> activities = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, userId);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Activity activity = new Activity();
                activity.setId(rs.getInt("id"));
                activity.setLabel(rs.getString("label"));
                activity.setIdUser(rs.getInt("idUser"));
                List<LocalDate> activityDays = dailyNewsRepo.getActivityDays(activity.getId());
                activity.setActivityDays(activityDays);
                activities.add(activity);
            }
        } catch (SQLException e) {
            logger.error("Failed to retrieve activities for user {}", userId, e);
            throw new CustomExceptions.DataAccessException("Erreur lors de la récupération des activités.", e);
        }
        return activities;
    }

    @Override
    public boolean add(Activity activity) {
        try (Connection connection = dataSource.getConnection()) {
            String sql = "INSERT INTO activity (label, idUser) VALUES (?, ?)";
            try (PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
                stmt.setString(1, activity.getLabel());
                stmt.setInt(2, activity.getIdUser());
                stmt.executeUpdate();

                ResultSet rs = stmt.getGeneratedKeys();
                if (rs.next()) {
                    activity.setId(rs.getInt(1));
                }
                return true;
            }
        } catch (SQLException e) {
            logger.error("Failed to add activity: {}", activity, e);
            return false;
        }
    }

    public List<Activity> getActivitiesByDate(LocalDate date) {
        String sql = "SELECT a.* FROM activity a JOIN dailyNews dn ON a.id = dn.idActivity WHERE dn.date = ?";
        List<Activity> activities = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setDate(1, Date.valueOf(date));
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Activity activity = sqlToActivity(rs);
                activities.add(activity);
            }
        } catch (SQLException e) {
            logger.error("Failed to retrieve activities for date {}", date, e);
            throw new CustomExceptions.DataAccessException("Erreur lors de la récupération des activités par date.", e);
        }
        return activities;
    }

    @Override
    public Activity getById(int id) {
        String sql = "SELECT a.*, dn.date FROM activity a LEFT JOIN dailyNews dn ON a.id = dn.idActivity WHERE a.id = ?";
        try (Connection connection = dataSource.getConnection();
             PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            Activity activity = null;
            List<LocalDate> activityDays = new ArrayList<>();

            while (rs.next()) {
                if (activity == null) {
                    activity = new Activity(
                            rs.getInt("id"),
                            rs.getString("label"),
                            rs.getInt("idUser"),
                            activityDays);
                }
                Date sqlDate = rs.getDate("date");
                if (sqlDate != null) {
                    activityDays.add(sqlDate.toLocalDate());
                }
            }
            return activity;
        } catch (SQLException e) {
            logger.error("Failed to retrieve activity with ID {}", id, e);
            throw new CustomExceptions.DataAccessException("Erreur lors de la récupération de l'activité.", e);
        }
    }

    public List<Activity> findActByRange(LocalDate startDate, LocalDate endDate, int userId) {
        String sql = "SELECT DISTINCT a.id, a.label " +
                     "FROM activity a " +
                     "JOIN dailyNews dn ON a.id = dn.idActivity " +
                     "WHERE dn.date BETWEEN ? AND ? AND a.idUser = ?";
        List<Activity> activities = new ArrayList<>();
    
        try (Connection connection = dataSource.getConnection();
             PreparedStatement stmt = connection.prepareStatement(sql)) {
    
            stmt.setDate(1, java.sql.Date.valueOf(startDate));
            stmt.setDate(2, java.sql.Date.valueOf(endDate));
            stmt.setInt(3, userId);
            ResultSet rs = stmt.executeQuery();
    
            while (rs.next()) {
                Activity activity = new Activity();
                activity.setId(rs.getInt("id"));
                activity.setLabel(rs.getString("label"));
                activity.setIdUser(userId);
    
                activities.add(activity);
            }
    
        } catch (SQLException e) {
            throw new CustomExceptions.DataAccessException("Erreur lors de la récupération des activités dans la plage spécifiée.", e);
        }
    
        return activities;
    }

    @Override
    public boolean update(Activity activity) {
        String sql = "UPDATE activity SET label=?, idUser=? WHERE id=?";
        try (Connection connection = dataSource.getConnection();
             PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setString(1, activity.getLabel());
            stmt.setInt(2, activity.getIdUser());
            stmt.setInt(3, activity.getId());
            return stmt.executeUpdate() > 0;
        } catch (SQLException e) {
            logger.error("Failed to update activity with ID {}", activity.getId(), e);
            throw new CustomExceptions.DataAccessException("Erreur lors de la mise à jour de l'activité.", e);
        }
    }

    @Override
    public boolean delete(int id) {
        String sql = "DELETE FROM activity WHERE id=?";
        try (Connection connection = dataSource.getConnection();
             PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, id);
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            logger.error("Failed to delete activity with ID {}", id, e);
            throw new CustomExceptions.DataAccessException("Erreur lors de la suppression de l'activité.", e);
        }
    }

    public boolean activityExistsForUser(int userId, String label) {
        String sql = "SELECT COUNT(*) FROM activity WHERE idUser = ? AND label = ?";
        try (Connection connection = dataSource.getConnection();
             PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, userId);
            stmt.setString(2, label);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return rs.getInt(1) > 0;
            }
        } catch (SQLException e) {
            logger.error("Failed to check if activity exists for user {}", userId, e);
            throw new CustomExceptions.DataAccessException("Erreur lors de la vérification de l'existence de l'activité.", e);
        }
        return false;
    }

    private Activity sqlToActivity(ResultSet rs) throws SQLException {
        return new Activity(
                rs.getInt("id"),
                rs.getString("label"),
                rs.getInt("idUser"),
                null);
    }
}