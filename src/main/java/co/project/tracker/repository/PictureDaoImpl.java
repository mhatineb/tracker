package co.project.tracker.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.project.tracker.entities.Picture;
import co.project.tracker.repository.interfaces.CrudDao;

@Repository
public class PictureDaoImpl implements CrudDao<Picture> {

    private static final Logger logger = LoggerFactory.getLogger(PictureDaoImpl.class);

    @Autowired
    private DataSource dataSource;

    @Override
    public boolean add(Picture picture) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO picture (src, day, idUser) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1,picture.getSrc());
            stmt.setDate(2,Date.valueOf(picture.getDay()));
            stmt.setInt(3,picture.getIdUser());
            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            rs.next();
            picture.setId((rs.getInt(1)));
            return true;
            
    } catch (SQLException e){
        logger.error("Failed to add picture due to SQL exception", e);

    }
        return false;
    }

    @Override
    public List<Picture> getAll() {
        List<Picture> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM picture");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                Picture picture = sqlToPicture(rs);
                list.add(picture);

            }
        } catch (SQLException e) {
            logger.error("Failed to getAll picture due to SQL exception", e);

        }
        return list;
    }

    @Override
    public Picture getById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM picture WHERE id=?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                Picture picture = sqlToPicture(rs);
                return picture;

            }
        } catch (SQLException e) {
            logger.error("Failed to getById picture due to SQL exception", e);

        }
        return null;
    }

    private Picture sqlToPicture(ResultSet rs) throws SQLException {
        return new Picture(
            rs.getInt("id"),
            rs.getString("src"),
            rs.getDate("day").toLocalDate(),
            rs.getInt("idUser")    
        );
    }

    @Override
    public boolean update(Picture picture) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE picture SET src=?,day=?,idUser=? WHERE id=?");
            stmt.setString(2, picture.getSrc());
            stmt.setDate(3,Date.valueOf(picture.getDay()));
            stmt.setInt(4, picture.getIdUser());

            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            logger.error("Failed to update picture due to SQL exception", e);

        }
        return false;
    }

    @Override
    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM picture WHERE id=?");
            stmt.setInt(1,id);

            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            logger.error("Failed to delete picture due to SQL exception", e);

        }
        return false;
    }

}
