package co.project.tracker.repository.interfaces;

import java.util.List;


public interface CrudDao<E> {
    default boolean add(E entity) {
        throw new UnsupportedOperationException("Add not implemented");
    }

    default List<E> getAll() {
        throw new UnsupportedOperationException("GetAll not implemented");
    }

    default List<E> getByUserId(int id) {
        throw new UnsupportedOperationException("GetById not implemented");
    }

    default E getById(int id) {
        throw new UnsupportedOperationException("GetById not implemented");
    }

    default boolean update(E entity) {
        throw new UnsupportedOperationException("Update not implemented");
    }

    default boolean delete(int id) {
        throw new UnsupportedOperationException("Delete not implemented");
    }
}
