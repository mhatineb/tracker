package co.project.tracker.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.project.tracker.entities.User;
import co.project.tracker.repository.interfaces.CrudDao;

@Repository
public class UserDaoImpl implements CrudDao<User> {
    
    private static final Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);
   
    @Autowired
    public DataSource dataSource;

    @Override
    public boolean add(User user) {    
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                "INSERT INTO user (email,password,username,role) VALUES (?,?,?,?)", 
                Statement.RETURN_GENERATED_KEYS
                );
            stmt.setString(1, user.getEmail());
            stmt.setString(2, user.getPassword());
            stmt.setString(3, user.getUsername());
            stmt.setString(4, user.getRole());
            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            rs.next();
            user.setId(rs.getInt(1));

            return true;
        } catch (SQLException e) {
            logger.error("Failed to add user with email {} due to SQL exception", user.getEmail(), e);
        }        
        return false;
    }

    @Override
    public List<User> getAll() {
        List<User> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM user");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                User user = sqlToUser(rs);
                list.add(user);
            }
        } catch (SQLException e) {
            logger.error("Failed to getAll users due to SQL exception", e);

        }
        return list;
    }

    @Override
    public User getById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM user WHERE id=?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                User user = sqlToUser(rs);
                return user;
            }
        } catch (SQLException e) {
            logger.error("Failed to getById users due to SQL exception", e);
        }
        return null;
    }
   
    private User sqlToUser(ResultSet rs) throws SQLException {
        return new User(
            rs.getInt("id"), 
            rs.getString("email"), 
            rs.getString("password"), 
            rs.getString("username"),
            rs.getString("role")
        );
    }

    @Override
    public boolean update(User user) {      
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                "UPDATE user SET email=?, password=?, username=?, role=? WHERE id=?"
            );
            stmt.setString(1, user.getEmail());
            stmt.setString(2, user.getPassword());
            stmt.setString(3, user.getUsername());
            stmt.setString(4, user.getRole());
            stmt.setInt(5, user.getId()); 
    
            int affectedRows = stmt.executeUpdate(); 
    
            // Retourner true si au moins une ligne a été affectée
            return affectedRows > 0;
        } catch (SQLException e) {
            logger.error("Failed to update user with ID {} due to SQL exception", user.getId(), e);
            return false;
        }
    }
    

    @Override
    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM user WHERE id=?");
            stmt.setInt(1,id);
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            logger.error("Failed to delete user due to SQL exception", e);
        }
        return false;
    }

    public Optional<User> findByEmail(String email) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM user WHERE email=?");
            stmt.setString(1, email);
            ResultSet rs = stmt.executeQuery();
            
            if (rs.next()) {
                User user = sqlToUser(rs);
                return Optional.of(user);
            }
            // Retourner Optional.empty() si aucun utilisateur n'est trouvé
            return Optional.empty();
            
        } catch (SQLException e) {
            logger.error("Failed to findByEmail user due to SQL exception", e);
            return Optional.empty(); // Retourner Optional.empty() en cas d'erreur
        }
    }
    
   
}
        

