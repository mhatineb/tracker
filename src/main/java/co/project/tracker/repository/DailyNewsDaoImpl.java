package co.project.tracker.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.project.tracker.controller.ActivityController;
import co.project.tracker.entities.Activity;
import co.project.tracker.entities.DailyNews;
import co.project.tracker.entities.User;
import co.project.tracker.repository.interfaces.CrudDao;
import co.project.tracker.service.DailyNewsService;

@Repository
public class DailyNewsDaoImpl implements CrudDao<DailyNews> {
    private static final Logger logger = LoggerFactory.getLogger(DailyNewsDaoImpl.class);

    @Autowired
    private DataSource dataSource;

    /**
     * Ajoute une nouvelle entrée de DailyNews dans la base de données.
     *
     * @param dailyNews L'objet DailyNews contenant les informations à insérer.
     * @return true si l'insertion a réussi, false sinon.
     * @throws SQLException Si une erreur SQL se produit pendant l'insertion.
     * 
     * Voir {@link DailyNewsService#createDailyNews(DailyNews)}
     */
    public boolean add(DailyNews dailyNews) {
        String insertDailyNewsSql = "INSERT INTO dailyNews (idUser, idActivity, date, rating) VALUES (?, ?, ?, ?)";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(insertDailyNewsSql, Statement.RETURN_GENERATED_KEYS);

            stmt.setInt(1, dailyNews.getIdUser());
            stmt.setInt(2, dailyNews.getIdActivity());
            stmt.setDate(3, java.sql.Date.valueOf(dailyNews.getDate()));
            stmt.setInt(4, dailyNews.getRating());
            stmt.executeUpdate();
            
            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                dailyNews.setId(rs.getInt(1));
            }
            return true;
        } catch (SQLException e) {
            logger.error("Failed to save DailyNews", e);
            return false;
        }
    }

    //Récupère la moyenne d'humeur d'un utilisateur noté de 1 à 5
    public Double getMonthlyMoodByUserId(int userId, int month, int year) {
        String sql = "SELECT avg_rating FROM user_monthly_mood WHERE idUser = ? AND month = ? AND year = ?";
        try (Connection connection = dataSource.getConnection();
                PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, userId);
            stmt.setInt(2, month);
            stmt.setInt(3, year);

            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return rs.getDouble("avg_rating");
            }
        } catch (SQLException e) {
            logger.error("Failed to retrieve monthly mood for User ID: {}, Month: {}, Year: {}", userId, month, year,
                    e);
        }
        return null;
    }

    @Override
    public List<DailyNews> getAll() {
        List<DailyNews> dailyNewsList = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM dailyNews");
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                DailyNews dailyNews = sqlToDailyNews(rs);
                dailyNewsList.add(dailyNews);
            }
        } catch (SQLException e) {
            logger.error("Failed to retrieve all DailyNews", e);
        }
        return dailyNewsList;
    }

    @Override
    public DailyNews getById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM dailyNews WHERE id = ?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return sqlToDailyNews(rs);
            }
        } catch (SQLException e) {
            logger.error("Failed to retrieve DailyNews by ID: {}", id, e);
        }
        return null;
    }

    public List<DailyNews> getByUserId(int userId) {
        List<DailyNews> dailyNewsList = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "SELECT * FROM dailyNews WHERE idUser = ?");
            stmt.setInt(1, userId);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                DailyNews dailyNews = sqlToDailyNews(rs);
                dailyNewsList.add(dailyNews);
            }
        } catch (SQLException e) {
            logger.error("Failed to retrieve DailyNews by User ID: {}", userId, e);
        }
        return dailyNewsList;
    }

    /**
     * Récupère les dates d'activité pour une activité spécifique.
     *
     * @param activityId L'ID de l'activité pour laquelle récupérer les dates.
     * @return Une liste de dates (LocalDate) où l'activité a été réalisée.
     * @throws SQLException Si une erreur SQL se produit.
     *
     *                      Cette méthode est utilisée dans le contrôleur pour
     *                      mettre à jour les jours d'activité d'une activité.
     *                      Voir
     *                      {@link ActivityController#update(int, Activity, User)}
     *                      et {@link ActivityController#getActivityDates pour plus
     *                      de détails.
     */
    public List<LocalDate> getActivityDays(int activityId) {
        List<LocalDate> days = new ArrayList<>();
        String sql = "SELECT date FROM dailyNews WHERE idActivity = ?";
        try (Connection connection = dataSource.getConnection();
                PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, activityId);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                days.add(rs.getDate("date").toLocalDate());
            }
        } catch (SQLException e) {
            logger.error("Failed to retrieve activity days", e);
        }
        return days;
    }

    @Override
    public boolean update(DailyNews dailyNews) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "UPDATE dailyNews SET idUser = ?, idActivity = ?, date = ?, rating = ? WHERE id = ?");
            stmt.setInt(1, dailyNews.getIdUser());
            stmt.setInt(2, dailyNews.getIdActivity());
            stmt.setDate(3, java.sql.Date.valueOf(dailyNews.getDate()));
            stmt.setInt(4, dailyNews.getRating());
            stmt.setInt(5, dailyNews.getId());

            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            logger.error("Failed to update DailyNews with ID: {}", dailyNews.getId(), e);
        }
        return false;
    }

    @Override
    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM dailyNews WHERE id = ?");
            stmt.setInt(1, id);
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            logger.error("Failed to delete DailyNews with ID: {}", id, e);
        }
        return false;
    }

    private DailyNews sqlToDailyNews(ResultSet rs) throws SQLException {
        DailyNews dailyNews = new DailyNews();
        dailyNews.setId(rs.getInt("id"));
        dailyNews.setIdUser(rs.getInt("idUser"));
        dailyNews.setIdActivity(rs.getInt("idActivity"));
        dailyNews.setDate(rs.getDate("date").toLocalDate());
        dailyNews.setRating(rs.getInt("rating"));
        return dailyNews;
    }
}
