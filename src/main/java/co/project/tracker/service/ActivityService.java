package co.project.tracker.service;

import static co.project.tracker.constant.ErrorMessages.ACTIVITY_ALREADY_REGISTERED;
import static co.project.tracker.constant.ErrorMessages.UNAUTHORIZED_USER;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import co.project.tracker.entities.Activity;
import co.project.tracker.entities.User;
import co.project.tracker.repository.ActivityDaoImpl;
import co.project.tracker.repository.DailyNewsDaoImpl;
import co.project.tracker.exception.CustomExceptions.BadRequestException;
import co.project.tracker.exception.CustomExceptions.ResourceNotFoundException;
import co.project.tracker.exception.CustomExceptions.UnauthorizedException;


@Service
@Transactional
public class ActivityService {

    @Autowired
    private ActivityDaoImpl activityRepo;

    @Autowired
    private DailyNewsDaoImpl dailyNewsRepo;


    
    public Activity getActivityWithDays(int activityId) {
        Activity activity = activityRepo.getById(activityId);
        if (activity == null) {
            throw new ResourceNotFoundException("Activité introuvable avec l'ID : " + activityId);
        }

        // Initialiser `activityDays` avec les dates des dailyNews
        List<LocalDate> activityDays = dailyNewsRepo.getActivityDays(activityId);
        activity.setActivityDays(activityDays);

        return activity;
    }


    public List<Activity> getActivitiesWithDaysByRange(LocalDate startDate, LocalDate endDate, int userId) {
        List<Activity> activities = activityRepo.findActByRange(startDate, endDate, userId);

        // Enrichir chaque activité avec ses jours associés
        for (Activity activity : activities) {
            List<LocalDate> activityDays = dailyNewsRepo.getActivityDays(activity.getId());

            // Filtrer les dates pour garder uniquement celles dans la plage
            List<LocalDate> filteredDays = activityDays.stream()
                    .filter(day -> !day.isBefore(startDate) && !day.isAfter(endDate))
                    .collect(Collectors.toList());

            activity.setActivityDays(filteredDays);
        }

        return activities;
    }

    public Activity createActivity(Activity activity, User user) {
        if(user == null){
            throw new UnauthorizedException(UNAUTHORIZED_USER);
        }
        activity.setIdUser(user.getId());
        boolean created = activityRepo.add(activity); 
        if(!created) {
            throw new BadRequestException(ACTIVITY_ALREADY_REGISTERED);
        } else {
            return activity; 
        }
    }

    public void addDateToActivity(int activityId, LocalDate date, int userId) {
        // Vérifier si l'activité existe
        Activity activity = activityRepo.getById(activityId);
        if (activity == null) {
            throw new ResourceNotFoundException("Activité introuvable avec l'ID : " + activityId);
        }

        // Vérifier si l'utilisateur est autorisé
        if (activity.getIdUser() != userId) {
            throw new UnauthorizedException("Vous n'êtes pas autorisé à modifier cette activité.");
        }

        // Vérifier si la date existe déjà
        List<LocalDate> activityDays = activity.getActivityDays();
        if (activityDays == null) {
            activityDays = new ArrayList<>();
        }

        if (!activityDays.contains(date)) {
            activityDays.add(date);
            activity.setActivityDays(activityDays);

            // Mettre à jour l'activité dans le repository
            if (!activityRepo.update(activity)) {
                throw new RuntimeException("Erreur lors de la mise à jour de l'activité.");
            }
        }
    }

    public List<Activity> getActivitiesByMonthAndYear(int userId, int month, int year) {
        List<Activity> activities = activityRepo.getMyActivities(userId);
    
        // Filtrer les jours associés pour chaque activité
        return activities.stream()
                .map(activity -> {
                    List<LocalDate> filteredDays = activity.getActivityDays().stream()
                            .filter(day -> day.getYear() == year && day.getMonthValue() == month)
                            .collect(Collectors.toList());
                    activity.setActivityDays(filteredDays);
                    return activity;
                })
                .filter(activity -> !activity.getActivityDays().isEmpty())
                .collect(Collectors.toList());
    }
}
