package co.project.tracker.service;

import static co.project.tracker.constant.ErrorMessages.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import co.project.tracker.entities.DailyNews;
import co.project.tracker.entities.User;
import co.project.tracker.exception.CustomExceptions.BadRequestException;
import co.project.tracker.exception.CustomExceptions.ResourceNotFoundException;
import co.project.tracker.exception.CustomExceptions.UnauthorizedException;
import co.project.tracker.repository.DailyNewsDaoImpl;

@Service
@Transactional
public class DailyNewsService {

    private final DailyNewsDaoImpl dailyNewsRepo;

    public DailyNewsService(DailyNewsDaoImpl dailyNewsRepo) {
        this.dailyNewsRepo = dailyNewsRepo;
    }

    public DailyNews createDailyNews(DailyNews dailyNews, User user) {
        if (user == null) {
            throw new UnauthorizedException(UNAUTHORIZED_USER);
        }
        dailyNews.setIdUser(user.getId());
        boolean created = dailyNewsRepo.add(dailyNews);
        if (!created) {
            throw new BadRequestException(DAILY_ALREADY_REGISTERED);
        }
        return dailyNews;
    }

    public List<DailyNews> getDailyNewsByUserId(User user) {
        if (user == null) {
            throw new UnauthorizedException(UNAUTHORIZED_USER);
        }
        return dailyNewsRepo.getByUserId(user.getId());
    }

    public Double getMonthlyMoodByUserId(User user, int month, int year) {
        if (user == null) {
            throw new UnauthorizedException(UNAUTHORIZED_USER);
        }
        Double avgMood = dailyNewsRepo.getMonthlyMoodByUserId(user.getId(), month, year);
        if (avgMood == null) {
            throw new ResourceNotFoundException("No mood data found for user in month " + month + " of year " + year);
        }
        return avgMood;
    }

    public Map<String, Double> getYearlyMoodByUserId(User user, int year) {
        if (user == null) {
            throw new UnauthorizedException(UNAUTHORIZED_USER);
        }
    
        int userId = user.getId();
        Map<String, Double> monthlyMoods = new HashMap<>();
        for (int month = 1; month <= 12; month++) {
            Double avgMood = dailyNewsRepo.getMonthlyMoodByUserId(userId, month, year);
            monthlyMoods.put(String.valueOf(month), avgMood != null ? avgMood : 0.0);
        }
        return monthlyMoods;
    }

    public void deleteDailyNews(int id) {
        boolean deleted = dailyNewsRepo.delete(id);
        if (!deleted) {
            throw new ResourceNotFoundException("DailyNews with id " + id + " not found.");
        }
    }
}