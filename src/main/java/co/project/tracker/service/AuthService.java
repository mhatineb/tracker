package co.project.tracker.service;

import java.util.Optional;
import static co.project.tracker.constant.ErrorMessages.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import co.project.tracker.entities.User;
import co.project.tracker.exception.CustomExceptions.BadRequestException;
import co.project.tracker.exception.CustomExceptions.UnauthorizedException;
import co.project.tracker.repository.UserDaoImpl;
import co.project.tracker.security.AuthResponse;
import co.project.tracker.security.Credentials;
import co.project.tracker.security.JwtUtils;
import co.project.tracker.security.UserService;


@Service
public class AuthService {

    private final PasswordEncoder hasher;
    private final UserService userService;

    private final UserDaoImpl userRepo;
    private final JwtUtils jwtUtils;

    public AuthService(PasswordEncoder hasher,UserService userService, UserDaoImpl userRepo, JwtUtils jwtUtils) {
        this.hasher = hasher;
        this.userService = userService;
        this.userRepo = userRepo;
        this.jwtUtils = jwtUtils;
    }

    public void register(User user) throws BadRequestException {       
        Optional<User> existingUser = userRepo.findByEmail(user.getEmail());
        if (existingUser.isPresent()) {
            throw new BadRequestException(USER_ALREADY_EXIST);
        }
        String hashedPassword = hasher.encode(user.getPassword());
        user.setPassword(hashedPassword);
        user.setRole("ROLE_USER");
        userRepo.add(user);
    }  


    public AuthResponse authenticate(Credentials credentials) {
        User user = (User) userService.loadUserByUsername(credentials.email);
        if (!hasher.matches(credentials.password, user.getPassword())) {
            throw new UnauthorizedException(UNAUTHORIZED_USER);
        }
        String token = jwtUtils.generateJwt(user);

        return new AuthResponse((User) user, token);
    }
}
