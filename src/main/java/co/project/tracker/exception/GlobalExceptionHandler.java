package co.project.tracker.exception;

import java.time.LocalDateTime;
import static co.project.tracker.constant.ErrorMessages.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import co.project.tracker.exception.CustomExceptions.BadRequestException;
import co.project.tracker.exception.CustomExceptions.DataAccessException;
import co.project.tracker.exception.CustomExceptions.ForbiddenException;
import co.project.tracker.exception.CustomExceptions.ResourceNotFoundException;
import co.project.tracker.exception.CustomExceptions.SuccessException;
import co.project.tracker.exception.CustomExceptions.UnauthorizedException;


@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {SuccessException.class})
    public ResponseEntity<ErrorMessage> handleSuccessException(SuccessException ex, WebRequest request) {
        ErrorMessage message;
        message = new ErrorMessage(
                HttpStatus.OK.value(),
                LocalDateTime.now(),
                ex.getMessage(),
                request.getDescription(false)
        );
        return new ResponseEntity<>(message, HttpStatus.OK);
    }
    
    @ExceptionHandler(value= {DataAccessException.class})
    public ResponseEntity<ErrorMessage> handleDataAccessException(DataAccessException ex, WebRequest request) {
        ErrorMessage message = new ErrorMessage(
            HttpStatus.INTERNAL_SERVER_ERROR.value(),
            LocalDateTime.now(),
            DATA_ACCESS_ERROR,
            request.getDescription(false)
        );
        return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {ResourceNotFoundException.class})
    public ResponseEntity<ErrorMessage> handleResourceNotFoundException(ResourceNotFoundException ex, WebRequest request) {
        ErrorMessage message = new ErrorMessage(
            HttpStatus.NOT_FOUND.value(),
            LocalDateTime.now(),
            RESOURCE_NOT_FOUND,
            request.getDescription(false)
        );
        return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
    }
    
    @ExceptionHandler(value = {UnauthorizedException.class})
    public ResponseEntity<ErrorMessage> handleUnauthorizedException(UnauthorizedException ex, WebRequest request) {
        ErrorMessage message = new ErrorMessage(
            HttpStatus.UNAUTHORIZED.value(),
            LocalDateTime.now(),
            UNAUTHORIZED_USER,
            request.getDescription(false)
        );
        return new ResponseEntity<>(message, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<ErrorMessage> handleBadRequestException(BadRequestException ex, WebRequest request) {
        ErrorMessage message = new ErrorMessage(
            HttpStatus.BAD_REQUEST.value(),
            LocalDateTime.now(),
            ex.getMessage(),
            request.getDescription(false)
        );
        return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ForbiddenException.class)
    public ResponseEntity<ErrorMessage> handleForbiddenException(ForbiddenException ex, WebRequest request) {
        ErrorMessage message = new ErrorMessage(
            HttpStatus.FORBIDDEN.value(),
            LocalDateTime.now(),
            FORBIDDEN_ACCESS,
            request.getDescription(false)
        );
        return new ResponseEntity<>(message, HttpStatus.FORBIDDEN);
    }
}
