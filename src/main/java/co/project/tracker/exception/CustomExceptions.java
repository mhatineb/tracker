package co.project.tracker.exception;

import java.sql.SQLException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class CustomExceptions {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public static class BadRequestException extends RuntimeException {
        private static final long serialVersionUID = 1L;
        
        public BadRequestException(String msg) {
            super(msg);
        }
    }
    
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public static class ResourceNotFoundException extends RuntimeException {
        private static final long serialVersionUID = 1L;
        
        public ResourceNotFoundException(String msg) {
            super(msg);
        }
    }
    
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public static class UnauthorizedException extends RuntimeException {
        private static final long serialVersionUID = 1L;
        
        public UnauthorizedException(String msg) {
            super(msg);
        }
    }
    
    @ResponseStatus(HttpStatus.OK)
    public static class SuccessException extends RuntimeException {
        private static final long serialVersionUID = 1L;

        public SuccessException(String msg) {
            super(msg);
        }
    }
    
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public static class DataAccessException extends RuntimeException {
        private static final long serialVersionUID = 1L;

        public DataAccessException(String msg, SQLException e) {
            super(msg, e);
        }
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    public static class ForbiddenException extends RuntimeException {
      private static final long serialVersionUID = 1L;

      public ForbiddenException(String message) {
          super(message);
      }
  }

}
