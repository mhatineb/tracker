package co.project.tracker.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.project.tracker.entities.Picture;
import co.project.tracker.exception.CustomExceptions.ResourceNotFoundException;
import co.project.tracker.repository.PictureDaoImpl;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/picture")
public class PictureController {
    @Autowired
    private PictureDaoImpl repo;

    @GetMapping
    public List<Picture> all() { 
        List<Picture> picture = repo.getAll();
        if(picture.isEmpty()){
            throw new ResourceNotFoundException("None activityDay found");
        }
        return picture;
    }

    @GetMapping("/{id}")
    public Picture one(@PathVariable int id) {
        Picture picture = repo.getById(id);
        if(picture == null) {
            throw new  ResourceNotFoundException("picture" + picture + "not found");

        }
        return picture;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Picture add(@Valid @RequestBody Picture picture) {
        if(repo.getById(picture.getIdUser()) == null) {
            throw new RuntimeException("idUser doesn't exist");

        }        
        repo.add(picture);
        return picture;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        if(!repo.delete(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PatchMapping("/{id}")
    public Picture update(@PathVariable int id, @RequestBody Picture picture) {
        Picture toUpdate = one(id);
        if(picture.getSrc() != null) {
            toUpdate.setSrc(picture.getSrc());
        }
        if(picture.getDay() != null) {
            toUpdate.setDay(picture.getDay());
        }
        if(picture.getIdUser() != null) {
            toUpdate.setIdUser(picture.getIdUser());
        }
        repo.update(toUpdate);
        return toUpdate;
    }
}
