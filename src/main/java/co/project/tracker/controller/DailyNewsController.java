package co.project.tracker.controller;

import java.time.Year;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import co.project.tracker.entities.DailyNews;
import co.project.tracker.entities.User;
import co.project.tracker.service.DailyNewsService;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/dailynews")
public class DailyNewsController {

    @Autowired
    private DailyNewsService dailyNewsService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public DailyNews postDailyNews(@Valid @RequestBody DailyNews dailyNews, @AuthenticationPrincipal User user) {
        return dailyNewsService.createDailyNews(dailyNews, user);
    }

    @GetMapping
    public List<DailyNews> getAllDailyNewsByUser(@AuthenticationPrincipal User user) {
        return dailyNewsService.getDailyNewsByUserId(user);
    }

    @GetMapping("/monthly-mood")
    public ResponseEntity<Map<String, Double>> getMonthlyMoodByUserId(
            @RequestParam int month,
            @RequestParam(required = false) Integer year,
            @AuthenticationPrincipal User user) {

        if (year == null) {
            year = Year.now().getValue();
        }
        Double avgMood = dailyNewsService.getMonthlyMoodByUserId(user, month, year);

        Map<String, Double> response = new HashMap<>();
        response.put("averageRating", avgMood);

        return ResponseEntity.ok(response);
    }

    @GetMapping("/yearly-mood")
    public ResponseEntity<Map<String, Double>> getYearlyMoodByUserId(
            @RequestParam(required = false) Integer year,
            @AuthenticationPrincipal User user) {

        if (year == null) {
            year = Year.now().getValue();
        }

        Map<String, Double> monthlyMoods = dailyNewsService.getYearlyMoodByUserId(user, year);

        return ResponseEntity.ok(monthlyMoods);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        dailyNewsService.deleteDailyNews(id);
    }
}