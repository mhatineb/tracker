package co.project.tracker.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import co.project.tracker.entities.User;
import co.project.tracker.exception.CustomExceptions.ResourceNotFoundException;
import co.project.tracker.repository.UserDaoImpl;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private UserDaoImpl repo;  

    @GetMapping
    public List<User> all() { 
        List<User> user = repo.getAll();
        if(user.isEmpty()) {
            throw new ResourceNotFoundException("None users found");
         } 
        return user;
    }

    @GetMapping("/{id}")
    public User one(@PathVariable int id) {
        User user = repo.getById(id);
        if(user == null) {
            throw new ResourceNotFoundException("user" + user + "not found");
        }
        return user;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public User add(@Valid @RequestBody User user) {
        
        repo.add(user);
        return user;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        if(!repo.delete(id)) {
            throw new ResourceNotFoundException("User with id " + id + " not found.");
        }
    }

    @PutMapping("/{id}")
    public User update(@PathVariable int id, @RequestBody User user) {
        User toUpdate = one(id);
        if(user.getEmail() != null) {
            toUpdate.setEmail(user.getEmail());
        }
        if(user.getPassword() != null) {
            toUpdate.setPassword(user.getPassword());
        }

        if(user.getUsername() != null) {
            toUpdate.setUsername(user.getUsername());
        }
        // Si la mise à jour échoue
        if (!repo.update(toUpdate)) {         
        }
        return toUpdate;
    }
}
