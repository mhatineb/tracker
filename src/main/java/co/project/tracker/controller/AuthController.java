package co.project.tracker.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.project.tracker.entities.User;
import co.project.tracker.security.AuthResponse;
import co.project.tracker.security.Credentials;
import co.project.tracker.service.AuthService;


    @RestController
    @RequestMapping("/api")
    public class AuthController {
    @Autowired
    private AuthService authService;
    
    @PostMapping("/register")
    public ResponseEntity<User> postUser(@RequestBody User user) {
        authService.register(user);
        return ResponseEntity.ok(user);
    }

    @PostMapping("/login")
    public ResponseEntity<AuthResponse> postLogin(@RequestBody Credentials credentials) {
        AuthResponse authResponse = authService.authenticate(credentials);
        return ResponseEntity.ok(authResponse);
    }

    @GetMapping("/account")
    public ResponseEntity<?> getAccount(@AuthenticationPrincipal User user) {
        if (user == null) {
            throw new AuthenticationCredentialsNotFoundException("User is not authenticated or session is invalid.");
        }

        // Retourne un objet avec l'ID, l'email et le rôle
        return ResponseEntity.ok(new AccountDTO(user.getId(), user.getEmail(), user.getAuthorities().toString()));
    }

    // DTO pour encapsuler les détails de l'utilisateur
    public static class AccountDTO {
        private int id;
        private String email;
        private String role;

        public AccountDTO(int id, String email, String role) {
            this.id = id;
            this.email = email;
            this.role = role;
        }

        public int getId() {
            return id;
        }

        public String getEmail() {
            return email;
        }

        public String getRole() {
            return role;
        }
    }
}


