package co.project.tracker.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import co.project.tracker.entities.Activity;
import co.project.tracker.entities.User;
import co.project.tracker.exception.CustomExceptions.ForbiddenException;
import co.project.tracker.repository.ActivityDaoImpl;
import co.project.tracker.service.ActivityService;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/activity")
public class ActivityController {
    @Autowired
    private ActivityService activityService;

    @Autowired
    private ActivityDaoImpl activityRepo;

    @GetMapping("/monthly-activities")
    public List<Activity> getMonthlyActivities(
            @AuthenticationPrincipal User user,
            @RequestParam int month,
            @RequestParam int year) {
        return activityService.getActivitiesByMonthAndYear(user.getId(), month, year);
    }
    
    @GetMapping("/myActivities")
    public List<Activity> getActivitiesForUser(@AuthenticationPrincipal User user) {
        return activityRepo.getMyActivities(user.getId());
    }

    

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Activity postActivityForUser(@Valid @RequestBody Activity activity, @AuthenticationPrincipal User user) {
        activityService.createActivity(activity, user);
        return activity;
    }

    @GetMapping("/{activityId}/dates")
    public ResponseEntity<List<LocalDate>> getActivityDates(
            @PathVariable int activityId,
            @AuthenticationPrincipal User user) {
        Activity activity = activityService.getActivityWithDays(activityId);

        if (!activity.getIdUser().equals(user.getId())) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        List<LocalDate> activityDays = activity.getActivityDays();
        return activityDays.isEmpty() ? ResponseEntity.noContent().build() : ResponseEntity.ok(activityDays);
    }

    @GetMapping("/byDate")
    @ResponseStatus(HttpStatus.OK)
    public List<Activity> getActivitiesByDate(
            @RequestParam("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) {
        return activityRepo.getActivitiesByDate(date);
    }

    @GetMapping("/range")
    public List<Activity> getActivitiesByRange(
            @RequestParam("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate,
            @AuthenticationPrincipal User user) {
        return activityService.getActivitiesWithDaysByRange(startDate, endDate, user.getId());
    }

    @GetMapping("/{id}")
    public Activity one(@PathVariable int id, @AuthenticationPrincipal User user) {
        Activity activity = activityRepo.getById(id);

        if (!activity.getIdUser().equals(user.getId())) {
            throw new ForbiddenException("You do not have permission to access this activity.");
        }
        return activity;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        activityRepo.delete(id);
    }

    @PutMapping("/{id}")
    public Activity update(@PathVariable int id,
            @RequestBody Activity activity,
            @AuthenticationPrincipal User user) {
        Activity toUpdate = activityRepo.getById(id);

        if (!toUpdate.getIdUser().equals(user.getId())) {
            throw new ForbiddenException("You do not have permission to update this activity.");
        }

        if (activity.getLabel() != null) {
            toUpdate.setLabel(activity.getLabel());
        }
        if (activity.getActivityDays() != null) {
            toUpdate.setActivityDays(activity.getActivityDays());
        }

        activityRepo.update(toUpdate);
        return toUpdate;
    }
}
