package co.project.tracker.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import co.project.tracker.entities.Invite;
import co.project.tracker.exception.CustomExceptions.ResourceNotFoundException;
import co.project.tracker.repository.InviteDaoImpl;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/invite")
public class InviteController {
    @Autowired
    private InviteDaoImpl repo;

    @GetMapping
    public List<Invite> all() { 
        
        return repo.getAll();
    }

    @GetMapping("/{id}")
    public Invite one(@PathVariable int id) {
        Invite invite = repo.getById(id);
        if(invite == null) {
            throw new ResourceNotFoundException("Invitation non trouvé");
        }
        return invite;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Invite add(@Valid @RequestBody Invite invite) {
    if (repo.getById(invite.getSender()) == null) {
        throw new ResourceNotFoundException("sender doesn't exist");
    }

    repo.add(invite);
    return invite;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        if(!repo.delete(id)) {
            throw new ResourceNotFoundException("Invite with id: " + id + "not found");
        }
    }

    @PatchMapping("/{id}")
    public Invite update(@PathVariable int id, @RequestBody Invite invite) {
        Invite toUpdate = one(id);
        if (toUpdate == null) {
            throw new ResourceNotFoundException("Invitation with id " + id + " not found.");
        }
        if(invite.getStatus() != null) {
            toUpdate.setStatus(invite.getStatus());
        }
        if(invite.getSender() != null) {
            toUpdate.setSender(invite.getSender());
        }

        if(invite.getRecipient() != null) {
            toUpdate.setRecipient(invite.getRecipient());
        }

        if(!repo.update(toUpdate)){

        }
        repo.update(toUpdate);
        return toUpdate;
    }
}
