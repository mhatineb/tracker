package co.project.tracker.security;

import static co.project.tracker.constant.ErrorMessages.*;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import co.project.tracker.repository.UserDaoImpl;

@Service
public class UserService implements UserDetailsService {
    private final UserDaoImpl userRepo;

    public UserService(UserDaoImpl userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {       
        return userRepo.findByEmail(email)
        .orElseThrow(() -> new UsernameNotFoundException(USER_NOT_FOUND));
    }
}

