package co.project.tracker.security;

public class Credentials {
    public String email;
    public String password;

    // Constructeur par défaut
    public Credentials() {}

    // Getters et Setters
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
