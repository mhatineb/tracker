package co.project.tracker.security;

import co.project.tracker.entities.User;

public class AuthResponse {
    private Integer id;
    private String email;
    private String role;
    private String token;

    public AuthResponse(User user, String token) {
        this.id = user.getId();  // ✅ On ne stocke que l’ID
        this.email = user.getEmail();  // ✅ On ne stocke que l’email
        this.role = user.getRole();  // ✅ On stocke le rôle
        this.token = token;  // ✅ On stocke le token
    }

    // Getters et Setters
    public Integer getId() { return id; }
    public String getEmail() { return email; }
    public String getRole() { return role; }
    public String getToken() { return token; }
}