package co.project.tracker.security;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

import co.project.tracker.entities.User;

@Component
public class JwtUtils {
    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expirationMs}")
    private long jwtExpirationMs;

    public String generateJwt(UserDetails userDetails) {
        User user = (User) userDetails;
        return JWT.create()
        .withClaim("id", user.getId())
        .withClaim("email",user.getEmail())
        .withClaim("role", user.getAuthorities().toString())
        .withIssuedAt(new Date())
        .withExpiresAt(new Date(System.currentTimeMillis()+ jwtExpirationMs))
        .sign(Algorithm.HMAC256(secret));
    }
    
    public String validateAndGetUsername(String token) {
        JWTVerifier verifier = JWT.require(Algorithm.HMAC256(secret)).build();
        DecodedJWT decoded = verifier.verify(token);
        return decoded.getClaim("email").asString();
    }
}

