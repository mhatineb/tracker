package co.project.tracker.constant;

public class ErrorMessages {

    public static final String UNAUTHORIZED_USER = "Un problème est survenu";
    public static final String USER_ALREADY_EXIST = "L'utilisateur existe déjà.";
    public static final String USER_NOT_FOUND = "Aucun utilisateur trouvé avec cet email.";
    public static final String ACTIVITY_ALREADY_REGISTERED = "L'activité est déjà enregistrée pour cette date";
    public static final String DAILY_ALREADY_REGISTERED = "La journée est déjà enregistrée pour cette date";
    public static final String RESOURCE_NOT_FOUND = "Ressource non trouvée.";
    public static final String FORBIDDEN_ACCESS = "Accès interdit.";
    public static final String DATA_ACCESS_ERROR = "Erreur d'accès aux données.";   
}
