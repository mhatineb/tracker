package co.project.tracker.entities;

import java.time.LocalDate;
import java.util.List;

public class Activity {
    private Integer id;
    private String label;
    private Integer idUser;
    private List<LocalDate> activityDays;
    
    public Activity() {
    }

    public Activity(Integer id, String label, Integer idUser, List<LocalDate> activityDays) {
        this.id = id;
        this.label = label;
        this.idUser = idUser;
        this.activityDays = activityDays;
    }

    public Integer getIdUser() {
        return idUser;
    }
    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }
    
    public List<LocalDate> getActivityDays() {
        return activityDays;
    }

    public void setActivityDays(List<LocalDate> activityDays) {
        this.activityDays = activityDays;
    }
}
