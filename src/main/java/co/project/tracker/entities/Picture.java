package co.project.tracker.entities;

import java.time.LocalDate;

public class Picture {
    private Integer id;
    private String src;
    private LocalDate day;
    private Integer idUser;

    public Picture() {
    }
    public Picture(Integer id, String src, LocalDate day, Integer idUser) {
        this.id = id;
        this.src = src;
        this.day = day;
        this.idUser = idUser;
    }
    public Picture(String src, LocalDate day, Integer idUser) {
        this.src = src;
        this.day = day;
        this.idUser = idUser;
    }
    public Integer getIdUser() {
        return idUser;
    }
    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }
    public LocalDate getDay() {
        return day;
    }
    public void setDay(LocalDate day) {
        this.day = day;
    }
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getSrc() {
        return src;
    }
    public void setSrc(String src) {
        this.src = src;
    }
    
}
