package co.project.tracker.entities;

import java.time.LocalDate;

public class ActivityDay {
    private Integer id;
    private LocalDate day;
    private Integer idActivity;
    
    public ActivityDay() {
    }

    public ActivityDay(LocalDate day, Integer idActivity) {
        this.day = day;
        this.idActivity = idActivity;
    }

    public ActivityDay(Integer id, LocalDate day, Integer idActivity) {
        this.id = id;
        this.day = day;
        this.idActivity = idActivity;
    }
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public LocalDate getDay() {
        return day;
    }
    public void setDay(LocalDate day) {
        this.day = day;
    }

    public Integer getIdActivity() {
        return idActivity;
    }

    public void setIdActivity(Integer idActivity) {
        this.idActivity = idActivity;
    }
    
}
