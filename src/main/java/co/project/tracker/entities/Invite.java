package co.project.tracker.entities;

public class Invite {
    private Integer id;
    private Integer sender;
    private Integer recipient;
    private String status;
  
    public Invite() {
    }
    public Invite(Integer id, Integer sender, Integer recipient, String status) {
        this.id = id;
        this.sender = sender;
        this.recipient = recipient;
        this.status = status;
    }
    public Invite(Integer sender, Integer recipient, String status) {
        this.sender = sender;
        this.recipient = recipient;
        this.status = status;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    
    public Integer getSender() {
        return sender;
    }
    public void setSender(Integer sender) {
        this.sender = sender;
    }
    public Integer getRecipient() {
        return recipient;
    }
    public void setRecipient(Integer recipient) {
        this.recipient = recipient;
    }
}
