package co.project.tracker.entities;

import java.time.LocalDate;

public class DailyNews {
    private Integer id;    
    private Integer idUser;
    private Integer idActivity;
    private LocalDate date;
    private Integer rating;

    // Constructeur vide
    public DailyNews() {}

    // Constructeur paramétré
    public DailyNews(Integer idUser, Integer idActivity, LocalDate date, Integer rating) {
        this.idUser = idUser;
        this.idActivity = idActivity;
        this.date = date;
        this.rating = rating;
    }

    // Getters et Setters
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        if (rating != null && (rating < 1 || rating > 5)) {
            throw new IllegalArgumentException("Rating must be between 1 and 5.");
        }
        this.rating = rating;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Integer getIdActivity() {
        return idActivity;
    }

    public void setIdActivity(Integer idActivity) {
        this.idActivity = idActivity;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}