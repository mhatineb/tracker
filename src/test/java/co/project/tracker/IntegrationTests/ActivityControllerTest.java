package co.project.tracker.IntegrationTests;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.anyInt;
import org.mockito.InjectMocks;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import co.project.tracker.controller.ActivityController;
import co.project.tracker.entities.Activity;
import co.project.tracker.entities.User;
import co.project.tracker.repository.ActivityDaoImpl;
import co.project.tracker.repository.UserDaoImpl;
import co.project.tracker.security.JwtFilter;
import co.project.tracker.service.ActivityService;

@WebMvcTest(ActivityController.class)
public class ActivityControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ActivityDaoImpl activityRepo;

    @MockBean
    private UserDaoImpl userRepo;

    @MockBean
    private JwtFilter JwtFilter;

    @MockBean
    private ActivityService activityService;

    @InjectMocks
    private ActivityController activityController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(activityController).build();
    }

    @Test
    void testGetActivitiesForUser_UserIsAuthenticated() throws Exception {
        
        User mockUser = new User();
        mockUser.setId(1);
        mockUser.setEmail("test@example.com");

        Activity activity1 = new Activity();
        activity1.setId(1);
        activity1.setLabel("Activity 1");
        Activity activity2 = new Activity();
        activity2.setId(2);
        activity2.setLabel("Activity 2");

        List<Activity> activities = Arrays.asList(activity1, activity2);

        when(activityRepo.getMyActivities(anyInt())).thenReturn(activities);

        mockMvc.perform(get("/api/activity/myActivities", mockUser)
                .principal(() -> "test@example.com") 
                .flashAttr("user", mockUser))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].label").value("Activity 1"))
                .andExpect(jsonPath("$[1].label").value("Activity 2"));
    }
}
