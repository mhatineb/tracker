package co.project.tracker.IntegrationTests;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import java.util.List;
import java.util.stream.Stream;
import co.project.tracker.controller.AuthController;
import co.project.tracker.entities.Activity;
import co.project.tracker.repository.ActivityDaoImpl;
import co.project.tracker.security.JwtFilter;
import co.project.tracker.security.JwtUtils;
import co.project.tracker.service.ActivityService;

import static org.junit.jupiter.api.Assertions.*;

@TestPropertySource(locations = "classpath:application-test.properties")
@SpringBootTest
@AutoConfigureMockMvc
class ActivityTntegrationTest {

    @Autowired
    private ActivityDaoImpl activityRepo;

    @InjectMocks
    private ActivityService activityService;

    @InjectMocks
    private AuthController authController;

    @MockBean
    private JwtUtils jwtUtils;

    @MockBean
    private JwtFilter jwtFilter;

    private static Stream<Object[]> provideActivitiesForUserAndExpectedResult() {
        return Stream.of(
                new Object[] { 1, 2 },
                new Object[] { 2, 1 });
    }

    @ParameterizedTest
    @MethodSource("provideActivitiesForUserAndExpectedResult")
    void testFindActivityForUser(int userId, int expectedResult) {

        List<Activity> activities = activityRepo.getMyActivities(userId);

        assertEquals(expectedResult, activities.size(), "Le nombre d'activités récupérées doit être correct.");

        if (!activities.isEmpty()) {
            assertNotNull(activities.get(0).getLabel(), "L'activité doit avoir un label.");
            assertTrue(activities.get(0).getId() > 0, "L'ID de l'activité doit être valide.");
        }
    }

}
