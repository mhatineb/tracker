package co.project.tracker.IntegrationTests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.project.tracker.controller.AuthController;
import co.project.tracker.entities.User;
import co.project.tracker.repository.UserDaoImpl;
import co.project.tracker.security.Credentials;
import co.project.tracker.security.JwtFilter;
import co.project.tracker.security.JwtUtils;
import co.project.tracker.security.UserService;
import co.project.tracker.service.AuthService;

@WebMvcTest(AuthController.class)
class AuthControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserDaoImpl userRepository;

    @MockBean
    private JwtUtils jwtUtils;

    @MockBean
    private JwtFilter jwtFilter;

    @MockBean
    private AuthService authService;
    
    @MockBean
    private UserService userService;

    @MockBean
    private PasswordEncoder hasher;

    @InjectMocks
    private AuthController authController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(authController).build();
    }

    @Test
    void testRegister() throws Exception {
        // Préparation d'un utilisateur à enregistrer
        User user = new User();
        user.setEmail("integrationtest@example.com");
        user.setPassword("plainPassword");
        user.setUsername("testuser");
        user.setRole("ROLE_USER");

        // Mocking the behavior of userRepository.findByEmail
        when(userRepository.findByEmail("integrationtest@example.com")).thenReturn(java.util.Optional.empty());

        // Mocking the behavior of passwordEncoder.encode
        when(hasher.encode(any(String.class))).thenReturn("hashedPassword");

        // Mocking the behavior of authService.register
        doAnswer(invocation -> {
            User userToRegister = invocation.getArgument(0);
            userToRegister.setPassword("hashedPassword");
            userToRegister.setRole("ROLE_USER");
            userRepository.add(userToRegister);
            return null;
        }).when(authService).register(any(User.class));

        // Mocking the behavior of userRepository.add
        doAnswer(invocation -> {
            User userToAdd = invocation.getArgument(0);
            when(userRepository.findByEmail(userToAdd.getEmail())).thenReturn(java.util.Optional.of(userToAdd));
            return null;
        }).when(userRepository).add(any(User.class));
        
        // Exécution de la requête HTTP POST pour l'enregistrement de l'utilisateur
        mockMvc.perform(post("/api/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(user)))
                .andExpect(status().isOk());

        // Vérification que l'utilisateur a été ajouté à la base de données
        assertTrue(userRepository.findByEmail("integrationtest@example.com").isPresent());
    }

    @Test
    void testLogin() throws Exception {
        // Création des credentials de connexion
        Credentials credentials = new Credentials();
        credentials.setEmail("integrationtest@example.com");
        credentials.setPassword("plainPassword");

        // Création d'un utilisateur simulé
        UserDetails mockUser = org.springframework.security.core.userdetails.User
                .withUsername("integrationtest@example.com")
                .password("hashedPassword")
                .roles("USER")
                .build();

        // Simulation de la récupération de l'utilisateur via AuthService
        when(userService.loadUserByUsername(credentials.getEmail())).thenReturn(mockUser);

        // Simulation du hash du mot de passe (comparaison réussie)
        when(hasher.matches(credentials.getPassword(), mockUser.getPassword())).thenReturn(true);

        // Simulation de la génération du token JWT
        String fakeJwtToken = "fake.jwt.token";
        when(jwtUtils.generateJwt(mockUser)).thenReturn(fakeJwtToken);

        // Exécution de la requête HTTP POST pour l'authentification
        mockMvc.perform(post("/api/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(credentials)))
                .andExpect(status().isOk()) 
                .andDo(result -> System.out.println("Full Response: " + result.getResponse().getContentAsString()));
            }
        }
