package co.project.tracker.IntegrationTests;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import co.project.tracker.controller.DailyNewsController;
import co.project.tracker.entities.DailyNews;
import co.project.tracker.entities.User;
import co.project.tracker.repository.ActivityDaoImpl;
import co.project.tracker.repository.DailyNewsDaoImpl;
import co.project.tracker.repository.UserDaoImpl;
import co.project.tracker.security.JwtFilter;
import co.project.tracker.service.DailyNewsService;

@WebMvcTest(DailyNewsController.class)
public class DailyNewsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DailyNewsDaoImpl dailyNewsRepo;

    @MockBean
    private DailyNewsService dailyNewsService;

    @MockBean
    private ActivityDaoImpl activityRepo;

    @MockBean
    private UserDaoImpl userRepo;

    @MockBean
    private JwtFilter jwtFilter;

    @InjectMocks
    private DailyNewsController dailyNewsController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(dailyNewsController).build();
    }

    @Test
    void testGetAllDailyNewsByUser_UserIsAuthenticated() throws Exception {
        // Mock de l'utilisateur authentifié
        User mockUser = new User();
        mockUser.setId(1);

        // Mock des données retournées par le repository
        DailyNews dailyNews1 = new DailyNews();
        dailyNews1.setId(1);
        dailyNews1.setIdUser(1);
        dailyNews1.setIdActivity(101);
        dailyNews1.setRating(5);

        DailyNews dailyNews2 = new DailyNews();
        dailyNews2.setId(2);
        dailyNews2.setIdUser(1);
        dailyNews2.setIdActivity(102);
        dailyNews2.setRating(4);

        List<DailyNews> dailyNewsList = Arrays.asList(dailyNews1, dailyNews2);

        // Mock du comportement du repository
        when(dailyNewsService.getDailyNewsByUserId(any(User.class))).thenReturn(dailyNewsList);

        // Exécution de la requête et assertions
        mockMvc.perform(get("/api/dailynews")
                .principal(() -> "test@example.com")
                .flashAttr("user", mockUser))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].idActivity").value(101))
                .andExpect(jsonPath("$[0].rating").value(5))
                .andExpect(jsonPath("$[1].id").value(2))
                .andExpect(jsonPath("$[1].idActivity").value(102))
                .andExpect(jsonPath("$[1].rating").value(4));
    }

}