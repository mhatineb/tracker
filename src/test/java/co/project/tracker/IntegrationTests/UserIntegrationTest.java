package co.project.tracker.IntegrationTests;

import static org.junit.jupiter.api.Assertions.*;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;

import co.project.tracker.controller.AuthController;
import co.project.tracker.entities.User;
import co.project.tracker.repository.UserDaoImpl;
import co.project.tracker.security.JwtFilter;
import co.project.tracker.security.JwtUtils;
import co.project.tracker.service.AuthService;

@TestPropertySource(locations = "classpath:application-test.properties")
@SpringBootTest
@AutoConfigureMockMvc
class UserIntegrationTest {

    @Autowired
    private UserDaoImpl userRepository;

    @InjectMocks
    private AuthService authService;

    @InjectMocks
    private AuthController authController;

    @MockBean
    private JwtUtils jwtUtils;

    @MockBean
    private JwtFilter jwtFilter;

    private static Stream<Object[]> provideUsersAndExpectedResult() {
        return Stream.of(
                new Object[] { new User(
                        "test@example.com",
                        "password",
                        "testuser",
                        "USER"),
                        true
                }, 
                new Object[] { new User(
                        "test2@example.com",
                        "password2",
                        "username2",
                        "USER"),
                        false
                }
        );
    }

    @ParameterizedTest
    @MethodSource("provideUsersAndExpectedResult")
    void testFindUser(User testUser, boolean expectedResult) {
        boolean isPresent = userRepository.findByEmail(
                testUser.getEmail()).isPresent();
        assertEquals(
                expectedResult, isPresent);
    }
}
