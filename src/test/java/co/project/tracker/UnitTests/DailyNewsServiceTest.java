package co.project.tracker.UnitTests;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

import co.project.tracker.entities.DailyNews;
import co.project.tracker.entities.User;
import co.project.tracker.exception.CustomExceptions.ResourceNotFoundException;
import co.project.tracker.exception.CustomExceptions.UnauthorizedException;
import co.project.tracker.repository.DailyNewsDaoImpl;
import co.project.tracker.service.DailyNewsService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

class DailyNewsServiceTest {

    @Mock
    private DailyNewsDaoImpl dailyNewsRepo;

    @InjectMocks
    private DailyNewsService dailyNewsService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testCreateDailyNews_UserIsAuthenticated() {
        User mockUser = new User();
        mockUser.setId(1);

        DailyNews dailyNews = new DailyNews();
        dailyNews.setIdUser(mockUser.getId());
        dailyNews.setIdActivity(101);
        dailyNews.setDate(LocalDate.now());
        dailyNews.setRating(5);

        when(dailyNewsRepo.add(dailyNews)).thenReturn(true);

        DailyNews result = dailyNewsService.createDailyNews(dailyNews, mockUser);

        assertNotNull(result, "La DailyNews ne doit pas être nulle après la création.");
        assertEquals(mockUser.getId(), result.getIdUser(), "L'ID de l'utilisateur doit correspondre.");
        assertEquals(101, result.getIdActivity(), "L'activité sélectionnée doit être correcte.");
        assertEquals(5, result.getRating(), "La note de l'humeur doit être correcte.");
        assertEquals(LocalDate.now(), result.getDate(), "La date doit correspondre à celle définie.");

        verify(dailyNewsRepo, times(1)).add(dailyNews);
    }

    @Test
    void testCreateDailyNews_UserIsNotAuthenticated() {
        // Arrange
        DailyNews dailyNews = new DailyNews();

        // Act & Assert
        assertThrows(UnauthorizedException.class, () -> dailyNewsService.createDailyNews(dailyNews, null));
    }

    @Test
    void testGetDailyNewsByUserId_UserIsAuthenticated() {
        // Arrange
        User mockUser = new User();
        mockUser.setId(1);

        DailyNews dailyNews1 = new DailyNews();
        dailyNews1.setId(1);
        dailyNews1.setIdUser(mockUser.getId());

        DailyNews dailyNews2 = new DailyNews();
        dailyNews2.setId(2);
        dailyNews2.setIdUser(mockUser.getId());

        List<DailyNews> mockList = Arrays.asList(dailyNews1, dailyNews2);
        when(dailyNewsRepo.getByUserId(mockUser.getId())).thenReturn(mockList);

        // Act
        List<DailyNews> result = dailyNewsService.getDailyNewsByUserId(mockUser);

        // Assert
        assertNotNull(result);
        assertEquals(2, result.size());
        verify(dailyNewsRepo, times(1)).getByUserId(mockUser.getId());
    }

    @Test
    void testGetDailyNewsByUserId_UserIsNotAuthenticated() {
        // Act & Assert
        assertThrows(UnauthorizedException.class, () -> dailyNewsService.getDailyNewsByUserId(null));
    }

    @Test
    void testGetYearlyMoodByUserId() {
        // Arrange
        User mockUser = new User();
        mockUser.setId(1);
        int year = 2025;

        when(dailyNewsRepo.getMonthlyMoodByUserId(mockUser.getId(), 1, year)).thenReturn(3.5);
        when(dailyNewsRepo.getMonthlyMoodByUserId(mockUser.getId(), 2, year)).thenReturn(4.0);
        when(dailyNewsRepo.getMonthlyMoodByUserId(mockUser.getId(), 3, year)).thenReturn(null); // Pas de données pour
                                                                                                // mars

        // Act
        Map<String, Double> result = dailyNewsService.getYearlyMoodByUserId(mockUser, year);

        // Assert
        assertNotNull(result);
        assertEquals(12, result.size());
        assertEquals(3.5, result.get("1"));
        assertEquals(4.0, result.get("2"));
        assertEquals(0.0, result.get("3"));
        verify(dailyNewsRepo, times(12)).getMonthlyMoodByUserId(eq(mockUser.getId()), anyInt(), eq(year));
    }

    @Test
    void testDeleteDailyNews_Success() {
        // Arrange
        int dailyNewsId = 1;
        when(dailyNewsRepo.delete(dailyNewsId)).thenReturn(true);

        // Act
        dailyNewsService.deleteDailyNews(dailyNewsId);

        // Assert
        verify(dailyNewsRepo, times(1)).delete(dailyNewsId);
    }

    @Test
    void testDeleteDailyNews_NotFound() {
        // Arrange
        int dailyNewsId = 1;
        when(dailyNewsRepo.delete(dailyNewsId)).thenReturn(false);

        // Act & Assert
        assertThrows(ResourceNotFoundException.class, () -> dailyNewsService.deleteDailyNews(dailyNewsId));
        verify(dailyNewsRepo, times(1)).delete(dailyNewsId);
    }
}