package co.project.tracker.UnitTests;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import co.project.tracker.entities.Activity;
import co.project.tracker.repository.ActivityDaoImpl;
import co.project.tracker.repository.DailyNewsDaoImpl;
import co.project.tracker.service.ActivityService;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ActivityServiceTest {

    @Mock
    private ActivityDaoImpl activityRepo;

    @Mock
    private DailyNewsDaoImpl dailyNewsRepo;

    @InjectMocks
    private ActivityService activityService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetActivityWithDays() {
        // Arrange: définir une activité et une date pour simuler `activityDays`
        int activityId = 123;
        int userId = 1;
        LocalDate testDate = LocalDate.of(2024, 11, 1);

        Activity mockActivity = new Activity(activityId, "Yoga Class", userId, new ArrayList<>());
        when(activityRepo.getById(activityId)).thenReturn(mockActivity);
        when(dailyNewsRepo.getActivityDays(activityId)).thenReturn(Arrays.asList(testDate));

        // Act: appeler la méthode à tester
        Activity resultActivity = activityService.getActivityWithDays(activityId);

        // Assert: vérifier que la date est présente dans activityDays
        assertNotNull(resultActivity);
        assertEquals(Arrays.asList(testDate), resultActivity.getActivityDays(),
                "La liste activityDays devrait contenir la date testDate.");
    }

    @Test
    public void testGetActivitiesWithDaysByRange() {
        // Données de test
        LocalDate startDate = LocalDate.of(2025, 1, 1);
        LocalDate endDate = LocalDate.of(2025, 1, 31);
        int userId = 1;

        // Mock des activités
        Activity activity1 = new Activity(1, "Running", userId, null);
        Activity activity2 = new Activity(2, "Cycling", userId, null);

        List<Activity> mockActivities = Arrays.asList(activity1, activity2);

        // Mock des dates associées
        when(activityRepo.findActByRange(startDate, endDate, userId)).thenReturn(mockActivities);
        when(dailyNewsRepo.getActivityDays(1)).thenReturn(Arrays.asList(
                LocalDate.of(2025, 1, 5),
                LocalDate.of(2025, 1, 10),
                LocalDate.of(2025, 2, 1) // Hors plage
        ));
        when(dailyNewsRepo.getActivityDays(2)).thenReturn(Arrays.asList(
                LocalDate.of(2025, 1, 15),
                LocalDate.of(2025, 1, 20)));

        // Appel de la méthode
        List<Activity> activities = activityService.getActivitiesWithDaysByRange(startDate, endDate, userId);

        // Assertions
        assertEquals(2, activities.size());

        // Vérifie les jours d'activité pour "Running"
        assertEquals(Arrays.asList(LocalDate.of(2025, 1, 5), LocalDate.of(2025, 1, 10)),
                activities.get(0).getActivityDays());

        // Vérifie les jours d'activité pour "Cycling"
        assertEquals(Arrays.asList(LocalDate.of(2025, 1, 15), LocalDate.of(2025, 1, 20)),
                activities.get(1).getActivityDays());
    }

}