package co.project.tracker.UnitTests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;

import co.project.tracker.entities.User;
import co.project.tracker.exception.CustomExceptions.BadRequestException;
import co.project.tracker.repository.UserDaoImpl;
import co.project.tracker.security.AuthResponse;
import co.project.tracker.security.Credentials;
import co.project.tracker.security.JwtUtils;
import co.project.tracker.security.UserService;
import co.project.tracker.service.AuthService;

@ExtendWith(MockitoExtension.class)
class AuthServiceTest {

    @InjectMocks
    AuthService authService;

    @Mock
    PasswordEncoder hasher;

    @Mock
    UserDaoImpl userRepo;

    @Mock
    Credentials credentials;

    @Mock
    private JwtUtils jwtUtils;

    @Mock
    private UserService userService;

    @Mock
    UserDetails mockUser;

    @Test
    void testRegister() throws Exception {
        Mockito.when(hasher.encode(
                "1234")).thenReturn(
                        "un-hash");

        User user = new User();
        user.setEmail("test@test.com");
        user.setPassword("1234");

        authService.register(user);

        assertEquals("un-hash",
                user.getPassword());
    }

    @Test
    void testRegisterFail() {
        // Simule l'existence d'un utilisateur avec l'adresse email fournie
        Mockito.when(userRepo.findByEmail("test@test.com")).thenReturn(Optional.of(new User()));

        User user = new User();
        user.setEmail("test@test.com");
        user.setPassword("1234");

        // Vérification que l'exception est bien levée
        assertThrows(BadRequestException.class, () -> authService.register(user));
    }

    @Test
    void testAuthenticateSuccess() {        
        Credentials credentials = new Credentials();
        credentials.setEmail("user@example.com");
        credentials.setPassword("password123");
 
        User mockUser = new User();
        mockUser.setId(1);
        mockUser.setEmail("user@example.com");
        mockUser.setPassword("hashedPassword");
        mockUser.setRole("ROLE_USER");

        // when(userRepo.findByEmail(credentials.getEmail())).thenReturn(Optional.of(mockUser));
        when(userService.loadUserByUsername(credentials.getEmail())).thenReturn(mockUser);

        when(hasher.matches(credentials.getPassword(), mockUser.getPassword())).thenReturn(true);

        when(jwtUtils.generateJwt(mockUser)).thenReturn("fake.jwt.token");

        AuthResponse response = authService.authenticate(credentials);

        assertNotNull(response);
        assertEquals("fake.jwt.token", response.getToken());
        assertEquals(1, response.getId());  
        assertEquals("user@example.com", response.getEmail());  
        assertEquals("ROLE_USER", response.getRole());
    }
}
