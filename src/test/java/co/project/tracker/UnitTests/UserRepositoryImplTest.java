package co.project.tracker.UnitTests;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import javax.sql.DataSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.anyString;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

import co.project.tracker.entities.User;
import co.project.tracker.repository.UserDaoImpl;


class UserRepositoryImplTest {
    
    @Mock
    private DataSource dataSource;

    @Mock
    private Connection connection;

    @Mock
    private PreparedStatement preparedStatement;

    @Mock
    private ResultSet resultSet;

    @InjectMocks
    private UserDaoImpl userRepository;

    @BeforeEach
    public void setUp() throws SQLException {
        MockitoAnnotations.openMocks(this);       
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
    }

    @Test
    void testFindByEmail_UserExists() throws SQLException {
        String email = "test@example.com";
        
        User expectedUser = new User();
        expectedUser.setEmail(email);
        
        // Mock the ResultSet to return the expected values
        when(preparedStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true);
        when(resultSet.getString("email")).thenReturn(email);
        
        User actualUser = userRepository.findByEmail(email).orElse(null);

        assertNotNull(actualUser);
        assertEquals(expectedUser.getEmail(), actualUser.getEmail());
    }

    @Test
    void testFindByEmail_UserDoesNotExist() throws SQLException {
        String email = "test@example.com";
        
        // Mock the ResultSet to simulate no results found
        when(preparedStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(false);

        Optional<User> result = userRepository.findByEmail(email);

        // Assert that the result is empty
        assertTrue(result.isEmpty());
    }

    @Test
    void testFindByEmail_SqlException() throws SQLException {
        String email = "test@example.com";

        // Simulate an SQLException when trying to get a connection
        when(dataSource.getConnection()).thenThrow(SQLException.class);

        Optional<User> result = userRepository.findByEmail(email);

        // Assert that the result is empty due to the exception
        assertTrue(result.isEmpty());
    }
}

