-- Insérer des données de test pour l'utilisateur
-- test-data.sql

-- Créer la table users
CREATE TABLE IF NOT EXISTS user (
    id INT PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(30),
    email VARCHAR(100) NOT NULL,
    password VARCHAR(100) NOT NULL,
    role VARCHAR(100)
);

-- Insérer un utilisateur
INSERT INTO user (id, username, email, password, role) VALUES (1, 'testuser', 'test@example.com', 'testpassword', 'USER');

INSERT INTO user (id, username, email, password, role) VALUES (2, 'testuser2', 'test@example2.com', 'testpassword', 'USER');

-- Créer la table activity
CREATE TABLE IF NOT EXISTS activity (
    id INT PRIMARY KEY AUTO_INCREMENT,
    label VARCHAR(100),
    idUser INT,
    FOREIGN KEY (idUser) REFERENCES user (id),
    UNIQUE (label, idUser)
);

-- Insérer des activités liées à l'utilisateur
INSERT INTO activity (label, idUser) VALUES ('Activity 1', 1); 
INSERT INTO activity (label, idUser) VALUES ('Activity 2', 1);

INSERT INTO activity (label, idUser) VALUES ('Activity 3', 2);

-- Créer la table user_activity
CREATE TABLE IF NOT EXISTS user_activity (
    user_id INT,
    activity_id INT,
    PRIMARY KEY (user_id, activity_id),
    FOREIGN KEY (user_id) REFERENCES user (id),
    FOREIGN KEY (activity_id) REFERENCES activity (id) ON DELETE CASCADE
);

INSERT INTO user_activity(user_id, activity_id) VALUES (1,1);
INSERT INTO user_activity(user_id, activity_id) VALUES (1,2);

