# Étape de construction
FROM maven:3.9.9-eclipse-temurin-21 AS builder
WORKDIR /app
COPY pom.xml .
COPY src ./src
RUN mvn clean package -DskipTests

# Étape d'exécution
FROM eclipse-temurin:21-jre AS runner
WORKDIR /app
COPY --from=builder /app/target/tracker-0.0.1-SNAPSHOT.jar /app/tracker.jar
EXPOSE 8080
CMD ["java", "-jar", "tracker.jar"]

